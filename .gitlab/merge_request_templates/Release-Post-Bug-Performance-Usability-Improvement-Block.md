/label ~"release post" ~"release post item" ~"Technical Writing"

The Release Post Manager will use this template to create separate Bug Fixes, Performance Improvements, and Usability Improvements MRs for the release post blog.

[Instructions for MR](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-usability-improvements-bugs-and-performance-improvements)

## Key dates & Review

- [ ] By the 10th, `@Release Post Manager` informs EMs/PMs/PDs to draft/submit Usability improvements, Bugs, or Performance improvements via this MR per [release post MR task list item](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Release-Post.md#release-post-item-creation-reminders-release_post_manager)
- [ ] On 15th, no earlier than 5pm PT, `@Release Post Manager` assigns MR to `@TW Lead` for review and applies label `in review`
     - If there are no usability improvements yet added, alert UX DRI `@vkarnes` in this MR and ask for direction.
     - If there are no bug fixes or performance improvements yet added, modify the  content of this MR, referencing bugs or performance improvements as appropriate. For example, if you have no performance improvement highlights, you will update the copy from

       > In GitLab 14.8, we're shipping performance improvements for issues, projects, milestones, and much more!

       to:

       > In GitLab 14.8, we're shipping performance improvements for issues, projects, milestones, and much more! You can see the full list [here](https://gitlab.com/gitlab-org/gitlab/-/issues?sort=created_date&state=closed&milestone_title=14.8&label_name[]=performance).

       Prior to merging the updated content, alert Product Operations DRI `@fseifoddini` with as FYI in this MR.
- [ ] By the 16th: `@TW Lead` reviews, applies the `ready` label and assigns  to `@Release Post Manager`
- [ ] By the 17th: `@Release Post Manager` merges the MR into the main / master branch, prior to final content assembly
